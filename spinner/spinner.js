import React from 'react'
import './spinner.scss'

const Spinner = ({isLoading, pending, pendingFilesMultiple}) => {
  return isLoading ? (
    <div
      className={
        pending || pendingFilesMultiple ? 'spinner-overlay-pending' : 'spinner-overlay'
      }
    >
      <div
        className={
          pending
            ? 'spinner-pending'
            : pendingFilesMultiple
            ? 'spinner-pending-files'
            : 'spinner'
        }
      ></div>
    </div>
  ) : null
}

export default Spinner
