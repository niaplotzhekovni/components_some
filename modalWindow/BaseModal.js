import React, {useEffect, useState, useRef} from 'react'
import ReactDOM from 'react-dom'
import ClickAwayListener from 'react-click-away-listener'
import './baseModal.scss'

const BaseModal = ({
  title,
  subtitle,
  isOpen,
  onClose,
  children,
  isClickAway,
  boldSubtitle,
  titlePadding,
  isLoaded,
}) => {
  const [watchForBrowserTab, setWatchForBrowserTab] = useState(true)
  const [modalScroll, setModalScroll] = useState(false)
  const modalRef = useRef(null)
  useEffect(() => {
    document.body.style.overflowY = isOpen ? 'hidden' : 'auto'
    if (modalRef.current?.clientHeight !== 1 && !isLoaded) {
      const modalHeight = modalRef.current?.clientHeight
      if (modalHeight + 100 > window.innerHeight) {
        setModalScroll(true)
      } else {
        setModalScroll(false)
      }
    }
  }, [isOpen, isLoaded])

  useEffect(() => {
    document.addEventListener('visibilitychange', function () {
      if (document.hidden) {
        setWatchForBrowserTab(false)
      }
    })
  }, [watchForBrowserTab])
  if (!isOpen) return null
  return ReactDOM.createPortal(
    <div
      className={
        modalScroll
          ? 'custom__base__modal__wrapper_scroll'
          : 'custom__base__modal__wrapper'
      }
    >
      <ClickAwayListener
        onClickAway={() => {
          setWatchForBrowserTab(true)
          watchForBrowserTab && isClickAway ? onClose() : false
        }}
      >
        <div className="custom__base__modal">
          {title ? (
            <div className="custom__base__modal__title">
              <h2 className={titlePadding ? 'title__padding' : ''}>{title}</h2>
              <div className="close__icon" onClick={() => onClose()}></div>
            </div>
          ) : null}
          {subtitle ? (
            <p
              className={
                boldSubtitle
                  ? 'boldSubtitle'
                  : false || titlePadding
                  ? 'titlePadding'
                  : false
              }
            >
              {subtitle}
            </p>
          ) : null}
          <div ref={modalRef}>{children}</div>
        </div>
      </ClickAwayListener>
    </div>,
    document.body
  )
}

export default BaseModal
