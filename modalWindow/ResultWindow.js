import React, {useEffect, useState} from 'react'
import ReactDOM from 'react-dom'
import ClickAwayListener from 'react-click-away-listener'
import './resultWindow.scss'
import Button from '../Button'

const ResultWindow = ({
  title,
  isOpen,
  onClose,
  children,
  isClickAway,
  titlePadding,
  erorIcon,
  onlyInform,
}) => {
  const [watchForBrowserTab, setWatchForBrowserTab] = useState(true)
  useEffect(() => {
    document.body.style.overflowY = isOpen ? 'hidden' : 'auto'
  }, [isOpen])

  useEffect(() => {
    document.addEventListener('visibilitychange', function () {
      if (document.hidden) {
        setWatchForBrowserTab(false)
      }
    })
  }, [watchForBrowserTab])
  if (!isOpen) return null
  return ReactDOM.createPortal(
    <div className="custom__result__modal__wrapper">
      <ClickAwayListener
        onClickAway={() => {
          setWatchForBrowserTab(true)
          watchForBrowserTab && isClickAway ? onClose() : false
        }}
      >
        <div className="custom__result__modal">
          <div className="custom__result__modal__title">
            <div className={erorIcon ? 'error__icon' : 'checked__icon'}></div>
            <div className="custom__result__modal__body">
              <h2 className={titlePadding ? 'title__padding' : false}>{title}</h2>
              {children}
            </div>
          </div>
          {!onlyInform && (
            <div className="custom__result__modal__button__close">
              <Button handlePush={onClose} text="Закрити" />
            </div>
          )}
        </div>
      </ClickAwayListener>
    </div>,
    document.body
  )
}

export default ResultWindow
