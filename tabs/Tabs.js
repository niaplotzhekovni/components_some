import React, {useEffect, useState} from 'react'
import './tabs.scss'
import {useNavigate} from 'react-router-dom'

const TabsNew = (props) => {
  const navigate = useNavigate()
  const [tabs, setTabs] = useState([])
  useEffect(() => {
    if (props.tabs.length) {
      setTabs(props.tabs)
    }
  }, [props.tabs])
  return (
    <div className="tab__container">
      {tabs.map((item, index) => {
        return (
          <button
            disabled={item.disabled}
            key={index}
            className={`${'tab'} ${
              props.activeTab === item.title
                ? 'active__tab'
                : item.disabled
                ? 'disabled_tab'
                : ''
            }`}
            onClick={() =>
              !item.disabled ? navigate(`${props.activeUrl}${item.title}`) : ''
            }
          >
            {item.tabName}
          </button>
        )
      })}
    </div>
  )
}

export default TabsNew
