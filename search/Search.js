import React, {useEffect, useState} from 'react'
import './search.css'
import ClickAwayListener from 'react-click-away-listener'
import i18n from '../../admin/locale/i18n'

const SearchNew = (props) => {
  const [isFocused, setIsFocused] = useState(false)
  const [text, setText] = useState('')
  const [timer, setTimer] = useState(null)

  useEffect(() => {
    setText(props.value || '')
  }, [props.value])

  const handleInputFocus = () => {
    setIsFocused(true)
  }
  const blurAway = () => {
    setIsFocused(false)
  }

  const change = (text) => {
    setText(text)
    if (timer) {
      clearTimeout(timer)
      setTimer(null)
    }
    setTimer(
      setTimeout(() => {
        props.onSearch(text)
      }, 1000)
    )
  }

  const defaultClassName = props.serchForTable
    ? 'container__search__for__table'
    : props.forSelect
    ? 'container_for_select_search'
    : props.allWidth
    ? 'container_all_width'
    : props.respondents
    ? 'container_all_width_respondents'
    : props.exclude
    ? 'container_all_width_exclude'
    : 'container__search'

  return (
    <ClickAwayListener onClickAway={blurAway}>
      <div className={defaultClassName + ' ' + (props.className ? props.className : '')}>
        <input
          id="search"
          placeholder={i18n.t('common.search')}
          value={text}
          onChange={(e) => change(e.target.value)}
          onFocus={handleInputFocus}
          type="search"
          name={props.name}
          autoFocus={props.focusAuto}
        />
        {isFocused && text ? (
          <span
            className="icon-clear"
            onClick={() => {
              change('')
            }}
          ></span>
        ) : (
          <span className="icon-search"></span>
        )}
      </div>
    </ClickAwayListener>
  )
}

export default SearchNew
