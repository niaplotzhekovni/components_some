import React from 'react'
import './importFileButton.scss'

const ImportFileButton = (props) => {
  return (
    <>
      <button
        disabled={props.disabled}
        className={
          props.buttonClsCancel
            ? 'button__light__file'
            : props.disabled
            ? 'button__disabled__file'
            : 'button__base__file'
        }
      >
        <label htmlFor={props.idItem ? props.idItem : 'fileLoad'} className="labelInput">
          {props.text}{' '}
        </label>
      </button>{' '}
      <input
        id={props.idItem ? props.idItem : 'fileLoad'}
        accept={props.accept}
        type="file"
        className="hideInput"
        onChange={(event) => {
          props.installFile(event)
        }}
      />
    </>
  )
}

export default ImportFileButton
