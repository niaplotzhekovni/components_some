import React, {useEffect, useState} from 'react'
import './select.css'
import ClickAwayListener from 'react-click-away-listener'

const Select = (props) => {
  const [isOpen, setIsOpen] = useState(false)
  useEffect(() => {
    console.log('typeChecked')
    if (!props.existValue) {
      console.log('typeChecked')
      props.onValueChange(props.options[0]?.props.value)
    }
  }, [])
  const toggleOptions = () => {
    setIsOpen(!isOpen)
  }

  const selectOption = (item) => {
    console.log('typeChecked', item)
    props.onValueChange(item)
    setIsOpen(false)
  }

  return (
    <div
      className={
        props.selectAuth ? 'custom-select-container-auth' : 'custom-select-container'
      }
    >
      <div className="select-wrapper">
        <div
          className={props.selectAuth ? 'selected-option-auth' : 'selected-option'}
          onClick={toggleOptions}
        >
          {props.iconCustom && (
            <span className="selected-dynamic-icon">
              <img src={props.iconCustom} alt="custom icon" />
            </span>
          )}
          <span className={props.selectAuth ? 'selected-auth-text' : 'selected-text'}>
            {props.value}
          </span>
          <span
            className={props.selectAuth ? 'selected-auth-icon' : 'selected-icon'}
          ></span>
        </div>
        {isOpen && (
          <ClickAwayListener onClickAway={() => setIsOpen(false)}>
            <div className="select-options">
              {props.options.map((el, index) => {
                return (
                  <div
                    key={index}
                    className="option"
                    onClick={() => selectOption(el.props.value)}
                  >
                    {el}
                  </div>
                )
              })}
            </div>
          </ClickAwayListener>
        )}
      </div>
    </div>
  )
}

export default Select
